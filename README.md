# PotatoBot
A dynamic module-based bot for Discord, written in Kotlin


### Text Functionality
This re-write of PotatoBot focuses on modularity and overall ease of customization. The current version contains the ability to:
- Use basic debugging/included commands like pinging the bot, checking information about other users/bots.
- Admin functionality to allow banning, kicking, and unbanning members.
- Module+Command framework that can be inherited on a per-module basis to cut down the amount of repeated code across modules.

It is intended to:
- Allow future maintainers and users alike to create new module+command groups with a very low barrier to entry by leveraging a built-in inheritance/framework structure that cuts down on repeated code.
- Allow for easy modularity across deployments. Bot users will be able to include or remove packages on a server-by-server basis via both human-readable config files and in-server configuration commands.
- Create a platform that is easy to maintain/expand. While end users will benefit from easily being able to create their own modules for the bot, this also cuts down my work as the developer. The existing framework on this bot allows me to very easily add new functionality and focus purely on the "function" part of new modules instead of having to implement the module/command handling each time.


### Voice Functionality
While PotatoBot does not have voice functionality yet, when it does it might include:
- Searching for and playing videos or music in voice chat from Youtube, Soundcloud, Spotify, Twitch, etc
- Support for easy creation of additional voice modules


### Project Direction
Why Kotlin?
- Kotlin provides a robust platform that is both easy to write and easy to maintain. Leveraging existing functionality from Java and JVM, while also utilizing Kotlin/KVM-specific libraries the possibilities for this project are near endless.

Why not use another Discord bot?
- This project was originally intended to be a small customized bot for use amongst friends in personal Discord servers. It has grown into a project for me to really test my limits with Kotlin and OOP in general. Additionally, by creating a modular-first bot in the way this project has now been laid out, I can add any and all future functionality I need for future Discord servers very easily.


## Quick Start Guide
In order to get started on hosting your own instance of this bot you'll need a few things. This information will assume that you are either running the bot on a unix-based system, or know how to fill in the gaps for your specific environment.


### Running a release binary
Required dependencies include
- Java JDK 8 or later. While I currently run OpenJDK 1.8.0_282, YMMV. This requirement should be pretty flexible.
- A [Discord](https://discord.com/developers/docs/intro) bot API Token

Steps to start include
- After you've satisfied the above requirements, download the latest release Jar file from [here](https://gitlab.com/Cyco12/PotatoBot/-/releases).
A folder called "modules" should be created in the same directory as this jar file. This folder should be populated with ModuleBase compliant modules, which can be found [here]().

- Note, eventually this module handling process will be automated like the rest of the configuration process, but for now, you **must manually clone the project's modules folder**. Failure to include modules in the bot's directory will prevent the bot from running.

- After you've installed everything above, running the jar will prompt you for information to automatically generate a config file. This will store your choices on which modules to load, and where to store your token.

### Compiling your own binary
Required dependencies included
- Java JDK 8 or later. While I currently run OpenJDK 1.8.0_282, YMMV. This requirement should be pretty flexible.
- A [Discord](https://discord.com/developers/docs/intro) bot API Token
- Kotlin version 1.4.20 or later
- Gradle version 6.8.3 or later

Steps to start include
- Clone this project locally. The project's build.gradle and gradlew should be in your top-level project folder.
- Compile a jar file with Gradle. This can be done with the ```-jar``` task from this project's build.gradle. Running ```./gradlew jar``` will satisfy this requirement.
- Your compiled jar will be stored in the project's folder ```build/libs```. This binary should be moved out to another directory, and installed with the same steps as provided above.
- Create a folder in the same directory as this jar file, called "modules". Populate this folder with ModuleBase compliant modules, either from the project's source code (which you should have cloned by this step), or by writing your own.
- Note, eventually this module handling process will be automated like the rest of the configuration process, but for now, you **must manually clone the project's modules folder**. Failure to include modules in the bot's directory will prevent the bot from running.
- After you've installed everything above, running the jar will prompt you for information to automatically generate a config file. This will store your choices on which modules to load, and where to store your token.
