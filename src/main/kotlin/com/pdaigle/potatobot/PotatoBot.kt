// PotatoBot 3.0.2
// Written by Phillip Daigle(Cyco12) 2020
// Main bot file
package com.pdaigle.potatobot

import com.pdaigle.potatobot.core.module.ModuleHandler
import com.pdaigle.potatobot.core.config.ConfigHandler
import com.pdaigle.potatobot.core.logging.Log
import com.pdaigle.potatobot.core.ClientData
import com.pdaigle.potatobot.core.config.PathData

import dev.kord.core.Kord
import dev.kord.core.on
import dev.kord.core.event.gateway.ReadyEvent


private val VERSION = "3.0.3"

// Paths
private var paths = PathData(
	config = "config.yaml",
	modules = "modules",
	token = "token.yaml",
	useConfigForToken = true
)


// Main
suspend fun main()
{
	val logger = Log()
	val conf = ConfigHandler(logger, paths)
	val overriddenPaths = conf.getOverriddenPaths()
	conf.startupCheck()
	val TOKEN = conf.getToken()

	val clientData = ClientData(Kord(TOKEN), "!", logger, overriddenPaths)
	val handler = ModuleHandler(clientData)

	logger.logGeneric("CORE", "VERSION", VERSION)
	clientData.client.on<ReadyEvent> { logger.logGeneric("CORE", "LOGIN", clientData.client.selfId.toString()) }

	handler.loadModules()
	val hasRequiredMods = handler.checkRequired()
	if(!hasRequiredMods)
		logger.logGeneric("ERROR", "STANDALONE_ERROR", "Bot is missing required modules")
	if(hasRequiredMods)
		clientData.client.login()
}