package com.pdaigle.potatobot.core.helper

import com.pdaigle.potatobot.core.ClientData
import com.pdaigle.potatobot.modules.util.Help

import dev.kord.core.entity.*
import dev.kord.common.entity.Permission


// Contains basis for help objects. 
// * Includes responses for all commands in a module, 
// * Main function for overriding values in responses
// * A helpQuery function to look for responses for a specific command
interface HelpBase
{
    var clientInfo: ClientData
    var responses: HashMap<String, String>
    var permissions: HashMap<String, List<Permission>>

    // Overrideable main() function. This should be overridden per Help object to define what command responses are.
    fun main()
    {
        responses["test"] = "This is a test response. If you're seeing this, something is probably broken."
        responses["helpbase"] = "There should be a pair like this in the module specific responses map that lists the commands in the module."
    }

    // TODO: Add fuzzy checking/search for queries
    fun helpQuery(cmdName: String): String
    {
        //msgParam.channel.createMessage(responseParam)
        // Allow for querying this Help object's list of commands, should return that help response for that command name.
        // This function definition obtains the command's name from a String representing the command name

        return responses.getValue(cmdName)
    }
    fun helpQuery(msgParam: Message)
    {
        //msgParam.channel.createMessage(commandData.invalidResponse)

        // Allow for querying this Help object's list of commands, should return that help response for that command name.
        // This function definition obtains the command's name from a Message object
        // This may not be necessary
    }

    fun permissionQuery(cmdName: String): String
    {
        var permissionsAsString = ""
        val responsePretext = "The required permissions for this are:"
        for (it in permissions.getValue((cmdName)))
            permissionsAsString = "$responsePretext ${it.toString()}"

        return permissionsAsString
    }

    suspend fun sendHelp(msgParam: Message, authorName: String, queryResponse: Help.QueryResponse)
    {
        msgParam.channel.createMessage("$authorName, ${queryResponse.help}")
        msgParam.channel.createMessage(queryResponse.permission)
    }
    suspend fun sendHelp(msgParam: Message, cmdName: String, localResponse: String = helpQuery(cmdName)) { msgParam.channel.createMessage(localResponse) }
    suspend fun sendHelp(msgParam: Message, response: String) { msgParam.channel.createMessage(response) }

    // compiler bug go brrrrrr - https://youtrack.jetbrains.com/issue/KT-22191 - https://discuss.kotlinlang.org/t/platform-declaration-clash/12613/4
    /*fun getResponses(): HashMap<String, String>
    {
        return responses 
    }*/
    fun getResponseKeys(): ArrayList<String> = ArrayList(responses.keys)
    fun getResponseValues(): ArrayList<String> = ArrayList(responses.values)
}