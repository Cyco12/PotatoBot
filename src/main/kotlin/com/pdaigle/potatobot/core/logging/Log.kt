package com.pdaigle.potatobot.core.logging

import com.pdaigle.potatobot.core.command.ArgsData
import com.pdaigle.potatobot.core.module.ModuleData

import dev.kord.core.entity.Message

import java.sql.Timestamp


class Log
{
    enum class Colors(val color: String) { RESET("\u001B[0m"), BLACK("\u001B[30m"), RED("\u001B[31m"), 
        GREEN("\u001B[32m"), YELLOW("\u001B[33m"), BLUE("\u001B[34m"), 
        PURPLE("\u001B[35m"), CYAN("\u001B[36m"), WHITE("\u001B[37m") }


    data class CategoryPair(
        val category: String,
        val usage: String
    )

    fun logGeneric(category: String, usageType: String, toLog: String)
    {
        val categoryPair = getCategoryPair(category,usageType,toLog)
        printCategoryPair(categoryPair)
    }

    suspend fun logCmd(cmdName: String, msgParam: Message)
    {
        val time = getTimestamp()
        val category = getCategory("CMD")

        val localAuthor = msgParam.author?.username + "#" + msgParam.author?.discriminator
        val localServerObj = msgParam.getGuild()
        val localServer = localServerObj.id

        val pretext = time + category + Colors.RESET.color
        println(pretext + Colors.GREEN.color + localAuthor + Colors.RESET.color + " sent the " + Colors.GREEN.color + cmdName + Colors.RESET.color + " command in [" + localServer + "]" + Colors.RESET.color) //+ " [" + localChannel + "]") // add server/channel/sharding info to this
    }

    suspend fun logCmd(cmdName: String, msgParam: Message, msgArgs: String)
    {
        val time = getTimestamp()
        val category = getCategory("CMD")

        val localAuthor = msgParam.author?.username + "#" + msgParam.author?.discriminator
        val localServerObj = msgParam.getGuild()
        val localServer = localServerObj.id

        val pretext = time + category + Colors.RESET.color
        println(pretext + Colors.GREEN.color + localAuthor + Colors.RESET.color + " sent the " + Colors.GREEN.color + cmdName + Colors.RESET.color + " command with " + Colors.GREEN.color + msgArgs + Colors.RESET.color + " args in [" + localServer + "]" + Colors.RESET.color) //+ " [" + localChannel + "]") // add server/channel/sharding info to this
    }

    suspend fun logCmd(cmdName: String, msgParam: Message, msgArgs: ArgsData)
    {
        val time = getTimestamp()
        val category = getCategory("CMD")

        val localAuthor = msgParam.author?.username + "#" + msgParam.author?.discriminator
        val localServerObj = msgParam.getGuild()
        val localServer = localServerObj.id

        val pretext = time + category + Colors.RESET.color
        println(pretext + Colors.GREEN.color + localAuthor + Colors.RESET.color + " sent the " + Colors.GREEN.color + cmdName + Colors.RESET.color + " command with these args " + Colors.GREEN.color + msgArgs.snowflakes + " / " + msgArgs.flags + " / " + msgArgs.genericArgs + Colors.RESET.color + " in [" + localServer + "]" + Colors.RESET.color) //+ " [" + localChannel + "]") // add server/channel/sharding info to this
    }

    fun logCli(input: String, output: String)
    {
        val time = getTimestamp()
        val category = getCategory("CLI")
        val pretext = time + category + Colors.RESET.color

        println("$pretext the input: $input returned the output: $output")
    }
    fun printCli(promptText: String, newLine: Boolean = true)
    {
        val time = getTimestamp()
        val category = getCategory("CLI")
        val pretext = time + category + Colors.RESET.color

        if (!newLine)
            print("$pretext  $promptText")
        if (newLine)
            println("$pretext  $promptText")
    }

    fun logModule(modDataParam: ModuleData)
    {
        val loadedState = when(modDataParam.isLoaded) {
            true -> Colors.PURPLE.color + "LOADED" + Colors.RESET.color
            false -> Colors.RED.color + "UNLOADED" + Colors.RESET.color
        }

        logGeneric("", "INFO", "The " + Colors.PURPLE.color + modDataParam.name.toUpperCase() + Colors.RESET.color + " module is currently " + "[" + loadedState + "]")
    }

    fun logReqModule(modDataParam: ModuleData)
    {
        var loadedState = ""

        if (modDataParam.isLoaded) loadedState = Colors.PURPLE.color + "LOADED" + Colors.RESET.color
        if (!modDataParam.isLoaded)
        {
            loadedState = Colors.RED.color + "UNLOADED" + Colors.RESET.color
            logGeneric("ERROR", "ERROR", "Missing Required Module")
        }

        logGeneric("CORE", "INFO","The " + Colors.PURPLE.color + modDataParam.name.toUpperCase() + Colors.RESET.color + " module is currently " + "[" + loadedState + "]")
    }

    private fun getTimestamp(): String { return "[" + Colors.CYAN.color + Timestamp(System.currentTimeMillis()) + Colors.RESET.color + "]" }

    private fun getCategoryPair(category: String = "", usage: String, toLog: String): CategoryPair
    {
        var localUsage = ""

        val localCategory = when (category) {
            "CORE"  -> "[" + Colors.YELLOW.color + "CORE" + Colors.RESET.color + "]"
            "CMD"   -> "[" + Colors.GREEN.color + "COMMAND" + Colors.RESET.color + "]"
            "CLI"   -> "[" + Colors.BLUE.color + "CLI" + Colors.RESET.color + "]"
            "ERROR" -> "[" + Colors.RED.color + "ERROR" + Colors.RESET.color + "]"
            ""      -> "[INFO]"
            else    -> "[INFO]"
        }

        when (usage) {
            "INFO"             -> localUsage = toLog
            "VERSION"          -> localUsage = "Currently running PotatoBot version [" + Colors.YELLOW.color + toLog + Colors.RESET.color + "]"
            "ERROR"            -> localUsage = Colors.RED.color + "Error: " + toLog + Colors.RESET.color + " | "
            "STANDALONE_ERROR" -> localUsage = Colors.RED.color + toLog + Colors.RESET.color
            "LOGIN"            -> localUsage = "Logged in as " + Colors.GREEN.color + toLog + Colors.RESET.color
        }

        return CategoryPair(localCategory, localUsage)
    }

    private fun getCategory(category: String): String
    {
        var localCategory = ""

        when (category) {
            "CORE"  -> localCategory = "[" + Colors.YELLOW.color + "CORE" + Colors.RESET.color + "]"
            "CMD"   -> localCategory = "[" + Colors.GREEN.color + "COMMAND" + Colors.RESET.color + "]"
            "CLI"   -> localCategory = "[" + Colors.BLUE.color + "CLI" + Colors.RESET.color + "]"
            "ERROR" -> localCategory = "[" + Colors.RED.color + "ERROR" + Colors.RESET.color + "]"
            ""      -> localCategory = "[INFO]"
        }

        return localCategory
    }

    private fun printCategoryPair(pair: CategoryPair)
    {
        val time = getTimestamp()
        println(time + pair.category + pair.usage)
    }
}