package com.pdaigle.potatobot.core

import com.pdaigle.potatobot.core.logging.Log
import com.pdaigle.potatobot.core.config.PathData

import dev.kord.core.Kord


// Contains basis for client parameters to be passed. 
// * Includes parameter to be passed for prefix, 
// * client parameter,
// * a logger parameter
data class ClientData(val client: Kord, val prefix: String, val logger: Log, val paths: PathData)