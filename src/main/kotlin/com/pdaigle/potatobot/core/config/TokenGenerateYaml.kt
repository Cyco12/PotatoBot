package com.pdaigle.potatobot.core.config

import com.charleskorn.kaml.Yaml
import com.pdaigle.potatobot.core.cli.CliBase
import com.pdaigle.potatobot.core.logging.Log

import java.io.File
import java.nio.charset.Charset


// This function needs to
// X Generate a token.yaml file and prompt the user to either input the token or add it to the file then restart
class TokenGenerateYaml(tokenPath: String, logObj: Log): CliBase
{
    private val localPath = tokenPath
    override val promptResponse = "File token.yaml is not present in PotatoBot's install directory. Would you like to generate one? (Y/N): "
    override val logger = logObj
    private var tokenConfirmed = false

    override suspend fun execute(cmd: String, logger: Log)
    {
        when {
            cmd.toLowerCase().startsWith("y") -> {
                val output = "Generating new token.yaml file"
                val fileName = "token.yaml"

                logger.printCli(output)
                val file = File(fileName)

                fun notEmpty(input: String)
                {
                    if (input.toLowerCase().startsWith("y"))
                    {
                        logger.printCli("Enter Token: ", false)
                        val tokenInput = readLine()
                        var outputToken = TokenData()
                        outputToken.token = tokenInput!!
                        val result = Yaml.default.encodeToString(TokenData.serializer(), outputToken)
                        file.writeText(result)
                    }
                    else
                        logger.printCli("Please manually add your token in plain text to $fileName and restart PotatoBot.")
                }
                fun retry()
                {
                    logger.printCli("Would you like to enter your unique token now? (Y/N): ", false)
                    val input = readLine()
                    if (!input.isNullOrEmpty())
                        notEmpty(input)
                    else
                        logger.printCli("Invalid or missing input. Please try again.")

                }

                fun getTokenInput()
                {
                    logger.printCli("Would you like to enter your unique token now? (Y/N): ", false)
                    val input = readLine()
                    if (!input.isNullOrEmpty())
                        notEmpty(input)
                }

                fun confirmTokenInput()
                {
                    logger.printCli("[${readToken()}] has been stored as your token. Is this correct? (Y/N): ", false)
                    val confirmInput = readLine()
                    if (confirmInput!!.toLowerCase().startsWith("y"))
                    {
                        tokenConfirmed = true
                        logger.printCli("Token saved.")
                    }
                    else if (confirmInput.toLowerCase().startsWith("n"))
                    {
                        tokenConfirmed = false
                        getTokenInput()
                    }
                }

                getTokenInput()
                confirmTokenInput()

                while (!tokenConfirmed)
                {
                    logger.printCli("Invalid or missing input. Please try again.")
                    retry()
                }

                logger.logCli(cmd, output)
            }
            cmd.toLowerCase().startsWith("n") -> {
                val output = "Skipping token.yaml generation."
                logger.printCli(output)
                logger.logCli(cmd, output)
            }
            else -> logger.logGeneric("ERROR", "ERROR", "Invalid option")
        }
    }

    fun checkTokenStatus(): Boolean { return tokenConfirmed }

    private fun readToken(): String
    {
        // Getting module data YAML file as string
        val encoding = Charset.defaultCharset()
        val encoded = File("token.yaml").readBytes()
        val fileAsString = String(encoded, encoding).trimIndent()

        //Serialize ModuleData object from yaml string and return it
        return Yaml.default.decodeFromString(TokenData.serializer(), fileAsString).token
    }
}