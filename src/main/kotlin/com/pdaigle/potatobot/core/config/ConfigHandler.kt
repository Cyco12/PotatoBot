package com.pdaigle.potatobot.core.config

import com.pdaigle.potatobot.core.module.ModuleBase
import com.pdaigle.potatobot.core.module.ModuleData
import com.pdaigle.potatobot.modules.util.UtilMod
import com.pdaigle.potatobot.modules.userinfo.UserinfoMod
import com.pdaigle.potatobot.modules.admin.AdminMod

import com.charleskorn.kaml.Yaml
import com.pdaigle.potatobot.core.ClientData
import com.pdaigle.potatobot.core.logging.Log

import java.io.File
import java.nio.charset.Charset
import kotlin.io.path.Path


class ConfigHandler(logger: Log, paths: PathData)
{
    // TODO: Find a more permanent solution for storing token value and similar private/environment variables
    // TODO: Verify cross-platform capabilities for paths, if necessary use something like Apache Commons
    private val INSTALL_DIR = File("").absolutePath
    private val DIR_HIERARCHY = listOf<String>(INSTALL_DIR, "src", "main", "kotlin", "com", "pdaigle", "potatobot")
    private val localPaths = PathData(paths.config, paths.modules, paths.token, paths.useConfigForToken)
    lateinit var overridePaths: PathData
    val TOKEN_ENV_VAR = "POTATOBOTTOKEN"

    var modules = HashMap<String, ModuleBase>()
    private var moduleNames = mutableListOf<String>()
    var required = HashMap<String, ModuleBase>()
    private val localLogger = logger
    private var tokenExists = false


    // Get and run all module class files.
    // Required modules should be added to the required map by default here
    // Other modules for the modules map should be added dynamically based off of config.yaml
    //      Modules read dynamically should override module-specific yaml files with loaded values from main config
    fun main(clientDataParam: ClientData)
    {
        required["util"] = UtilMod(clientDataParam)
        getModulesMap(clientDataParam)
        loadConfig()
    }

    suspend fun startupCheck()
    {
        val checks = mutableMapOf<String, Boolean>()
        val localModules = getModulesMap(false)

        // check for token in environment variable, if not present, continue to other checks
        val tokenEnvVar: String = System.getenv(TOKEN_ENV_VAR) ?: "default_value"
        if (!tokenEnvVar.isNullOrEmpty() && tokenEnvVar != "default_value" && checks["token"] != true)
        {
            setToken(tokenEnvVar)
            checks["token"] = true
            tokenExists = true
        }

        // check for token in config.yaml, if not present, continue
        if ((File(localPaths.config).exists() || File("config.yaml").exists()) && localPaths.useConfigForToken)
        {
            val localToken = parseToken(File(localPaths.config))
            if (localToken.length >= 59)
            {
                checks["token"] = true
                tokenExists = true
            }
        }

        when {
            this::overridePaths.isInitialized -> {
                // check for token in token.yaml/token.txt, if not present, give user option to generate. if not generated, continue to other checks
                if ((!(File(overridePaths.token).exists()) && checks["token"] != true) && !overridePaths.useConfigForToken)
                {
                    if (!File("token.yaml").exists())
                    {
                        val localTokenGen = TokenGenerateYaml(overridePaths.token, localLogger)
                        localTokenGen.main()
                        if (localTokenGen.checkTokenStatus())
                        {
                            checks["token"] = true
                            tokenExists = true
                        }
                    }
                    if (!File("token.txt").exists() && !File("token.yaml").exists())
                    {
                        val localTokenGen = TokenGenerateTxt(overridePaths.token, localLogger)
                        localTokenGen.main()
                        if (localTokenGen.checkTokenStatus())
                        {
                            checks["token"] = true
                            tokenExists = true
                        }
                    }
                }

                when
                {
                    checks["token"] == true -> {
                        if (overridePaths.useConfigForToken)
                            localLogger.logGeneric("", "INFO", "Token found in ${overridePaths.config}")
                        else
                            localLogger.logGeneric("", "INFO", "Token found in ${overridePaths.token}")
                    }

                    checks.all { it.value } -> {

                        localLogger.logGeneric("", "INFO", "Startup check completed")
                    }
                }
            }

            !this::overridePaths.isInitialized -> {
                if(!File(localPaths.config).exists() && !File("config.yaml").exists())
                {
                    val localConfGen = ConfigGenerate(localPaths.config, localModules, localLogger)
                    localConfGen.main()
                    checks["config"] = true
                }

                // check for token in token.yaml/token.txt, if not present, give user option to generate. if not generated, continue to other checks
                if ((!(File(localPaths.token).exists()) && checks["token"] != true) && !localPaths.useConfigForToken)
                {
                    if (!File("token.yaml").exists())
                    {
                        val localTokenGen = TokenGenerateYaml(localPaths.token, localLogger)
                        localTokenGen.main()
                        if (localTokenGen.checkTokenStatus())
                        {
                            checks["token"] = true
                            tokenExists = true
                        }
                    }
                    if (!File("token.txt").exists() && !File("token.yaml").exists())
                    {
                        val localTokenGen = TokenGenerateTxt(localPaths.token, localLogger)
                        localTokenGen.main()
                        if (localTokenGen.checkTokenStatus())
                        {
                            checks["token"] = true
                            tokenExists = true
                        }
                    }
                }

                /*if(!File(MODULE_PATH).exists() && !File("modules").exists())
                {
                    val localModGen = ModulesGenerate(MODULE_PATH)
                    localModGen.main()
                    checks["modules"] = true
                }*/

                when
                {
                    checks["token"] == true -> {
                        if (localPaths.useConfigForToken)
                            localLogger.logGeneric("", "INFO", "Token found in ${localPaths.config}")
                        else
                            localLogger.logGeneric("", "INFO", "Token found in ${localPaths.token}")
                    }

                    checks.all { it.value } -> {

                        localLogger.logGeneric("", "INFO", "Startup check completed")
                    }
                }
            }
        }
    }

    fun getToken(): String
    {
        if (!tokenExists)
        {
            localLogger.logGeneric("ERROR", "ERROR", ("Token cannot be found or does not exist in ${localPaths.token}"))
            return ""
        }
        if ((localPaths.token == "token.txt" || localPaths.token == "token.yaml") && !localPaths.useConfigForToken)
        {
            val localToken = parseToken(File(localPaths.token))

            if (localToken.length >= 59)
                return localToken
            else
                localLogger.logGeneric("ERROR", "ERROR", "Invalid Token. Please update ${localPaths.token} and restart PotatoBot.")
        }
        if (localPaths.useConfigForToken)
        {
            val tokenInput = parseToken(File(localPaths.config))
            if (tokenInput.length >= 59)
                return tokenInput
            else
                localLogger.logGeneric("ERROR", "ERROR", "Invalid Token. Please update ${localPaths.token} and restart PotatoBot.")
        }
        return ""
    }

    private fun setToken(token: String) { if (File(localPaths.token).exists()) { File(localPaths.token).writeText(token) } }

    // yaml compliant map pre-client initialization
    private fun getModulesMap(withRequired: Boolean): MutableList<ConfigData.Module>
    {
        var localModulesList = mutableListOf<ConfigData.Module>()
        // populate list of names of every module included in the com.pdaigle.potatobot.modules package
        File(localPaths.modules).walk().forEach {
            if (it.extension == "yaml")
            {
                val moduleObj = parseModData(it)
                if (withRequired)
                    localModulesList.add(ConfigData.Module(moduleObj.name, moduleObj.isLoaded))
                else if (!withRequired && !moduleObj.isRequired)
                    localModulesList.add(ConfigData.Module(moduleObj.name, moduleObj.isLoaded))
            }
        }

        return localModulesList
    }
    private fun getModulesMap(clientDataParam: ClientData)
    {
        // populate list of names of every module included in the com.pdaigle.potatobot.modules package
        File(localPaths.modules).walk().forEach {
            if (it.extension == "yaml")
            {
                val moduleObj = parseModData(it)
                if (moduleObj.isLoaded)
                    moduleNames.add(moduleObj.name)
            }
        }

        // add module class to modules[] list with moduleName as the key and module(ClientDataParam) as the value
        for (it in moduleNames)
        {
            when(it) {
                "AdminMod"    -> modules["AdminMod"] = AdminMod(clientDataParam)
                "UserinfoMod" -> modules["UserinfoMod"] = UserinfoMod(clientDataParam)
            }
        }
    }

    fun getOverriddenPaths(): PathData
    {
        // check for token in config.yaml, if not present, continue
        if ((File(localPaths.config).exists() || File("config.yaml").exists()) && localPaths.useConfigForToken)
        {
            val newPaths = when {
                File(localPaths.config).exists() -> parsePaths(File(localPaths.config))
                !File(localPaths.config).exists() && File("config.yaml").exists() -> parsePaths(File("config.yaml"))
                else -> PathData(localPaths.config, localPaths.modules, localPaths.token, localPaths.useConfigForToken)
            }

            println(newPaths != localPaths)
            if (newPaths != localPaths && newPaths != PathData("", "", "", true))
                overridePaths = newPaths
        }

        localLogger.logGeneric("CORE","INFO", "Override Paths in use: ${this::overridePaths.isInitialized}")

        if (this::overridePaths.isInitialized)
            return overridePaths
        else
            return localPaths
    }

    private fun loadConfig()
    {
        val configAsString = File(localPaths.config).inputStream().readBytes().toString(Charsets.UTF_8)
        val configObj = Yaml.default.decodeFromString(ConfigData.serializer(), configAsString)
        val configModuleMap = configObj.getAsMap(configObj.modules)
        for (it in configModuleMap.keys)
        {
            if (modules.keys.contains(it))
                modules[it]!!.modData.isLoaded = configModuleMap[it]!!
        }
    }

    // Parses module data from yaml file using a java.io.File
    private fun parseModData(yamlFile: File): ModuleData // Using passed string to get File object for module object
    {
        // Getting module data YAML file as string
        val encoding = Charset.defaultCharset()
        val encoded = yamlFile.readBytes()
        val fileAsString = String(encoded, encoding).trimIndent()

        //Serialize ModuleData object from yaml string and return it
        return Yaml.default.decodeFromString(ModuleData.serializer(), fileAsString)
    }

    private fun parseToken(yamlFile: File): String
    {
        // Getting config data YAML file as string
        val encoding = Charset.defaultCharset()
        val encoded = yamlFile.readBytes()
        val fileAsString = String(encoded, encoding).trimIndent()

        //Serialize ConfigData (or TokenData) object from yaml string and return it
        if (localPaths.useConfigForToken)
            return Yaml.default.decodeFromString(ConfigData.serializer(), fileAsString).token
        else
            return Yaml.default.decodeFromString(TokenData.serializer(), fileAsString).token
    }

    private fun parsePaths(yamlFile: File): PathData
    {
        // Getting config data YAML file as string
        val encoding = Charset.defaultCharset()
        val encoded = yamlFile.readBytes()
        val fileAsString = String(encoded, encoding).trimIndent()

        //Serialize ConfigData object from yaml string and return it
        return Yaml.default.decodeFromString(ConfigData.serializer(), fileAsString).getPaths()
    }
}