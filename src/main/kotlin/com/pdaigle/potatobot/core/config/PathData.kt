package com.pdaigle.potatobot.core.config

import kotlinx.serialization.Serializable

@Serializable
data class PathData(
    val config: String,
    val modules: String,
    val token: String,
    val useConfigForToken: Boolean
)
