package com.pdaigle.potatobot.core.config

import kotlinx.serialization.Serializable

@Serializable
class TokenData
{
    @Serializable
    lateinit var token: String
}