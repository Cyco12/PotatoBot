package com.pdaigle.potatobot.core.config

import com.pdaigle.potatobot.core.cli.CliBase
import com.pdaigle.potatobot.core.logging.Log

import java.io.File


// This function needs to
// X Generate a token.txt file and prompt the user to either input the token or add it to the file then restart
class TokenGenerateTxt(tokenPath: String, logObj: Log): CliBase
{
    private val localPath = tokenPath
    override val promptResponse = "File token.txt is not present in PotatoBot's install directory. Would you like to generate one? (Y/N): "
    override val logger = logObj
    private var tokenConfirmed = false

    override suspend fun execute(cmd: String, logger: Log)
    {
        when {
            cmd.toLowerCase().startsWith("y") -> {
                val output = "Generating new token.txt file"
                val fileName = "token.txt"
                // TODO: Create default path/structure; for now the file is generating in the same top-level directory as the jar
                //val filePath = localPath

                logger.printCli(output)
                val file = File(fileName)

                fun notEmpty(input: String)
                {
                    if (input.toLowerCase().startsWith("y"))
                    {
                        logger.printCli("Enter Token: ", false)
                        val tokenInput = readLine()
                        file.writeText(tokenInput!!)
                    }
                    else
                        logger.printCli("Please manually add your token in plain text to token.txt and restart PotatoBot.")
                }
                fun retry()
                {
                    logger.printCli("Would you like to enter your unique token now? (Y/N): ", false)
                    val input = readLine()
                    if (!input.isNullOrEmpty())
                        notEmpty(input)
                    else
                    {
                        logger.printCli("Invalid or missing input. Please try again.")
                    }
                }

                fun getTokenInput()
                {
                    logger.printCli("Would you like to enter your unique token now? (Y/N): ", false)
                    val input = readLine()
                    if (!input.isNullOrEmpty())
                        notEmpty(input)
                }

                fun confirmTokenInput()
                {
                    logger.printCli("[${File("token.txt").readText()}] has been stored as your token. Is this correct? (Y/N): ", false)
                    val confirmInput = readLine()
                    if (confirmInput!!.toLowerCase().startsWith("y"))
                    {
                        tokenConfirmed = true
                        logger.printCli("Token saved.")
                    }
                    else if (confirmInput.toLowerCase().startsWith("n"))
                    {
                        tokenConfirmed = false
                        getTokenInput()
                    }
                }

                getTokenInput()
                confirmTokenInput()

                while (!tokenConfirmed)
                {
                    logger.printCli("Invalid or missing input. Please try again.")
                    retry()
                }

                logger.logCli(cmd, output)
            }
            cmd.toLowerCase().startsWith("n") -> {
                val output = "Please manually create a file called 'token.txt' and Please manually add your token in plain text to token.txt and restart PotatoBot."
                logger.printCli(output)
                logger.logCli(cmd, output)
            }
            else -> logger.logGeneric("ERROR", "ERROR","Invalid option")
        }
    }

    fun checkTokenStatus(): Boolean { return tokenConfirmed }
}