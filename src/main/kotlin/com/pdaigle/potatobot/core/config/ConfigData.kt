package com.pdaigle.potatobot.core.config

import kotlinx.serialization.Serializable

import com.pdaigle.potatobot.core.config.PathData
import java.io.File

@Serializable
class ConfigData
{
    @Serializable
    data class Module(
        val name: String,
        val isLoaded: Boolean
    )

    @Serializable
    data class Path(
        val name: String,
        val path: String
    )

    @Serializable
    lateinit var modules: List<Module>

    @Serializable
    lateinit var paths: List<Path>

    @Serializable
    lateinit var token: String

    @Serializable
    var useConfigForToken: Boolean = true


    fun getAsMap(modulesList: List<Module>): HashMap<String, Boolean>
    {
        val localModuleMap = HashMap<String, Boolean>()
        for (it in modulesList)
            localModuleMap[it.name] = it.isLoaded

        return localModuleMap
    }

    fun getPaths(): PathData { return PathData(paths[0].path, paths[1].path, paths[2].path, useConfigForToken) }
}