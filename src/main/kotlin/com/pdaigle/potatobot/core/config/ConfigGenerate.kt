package com.pdaigle.potatobot.core.config

import com.charleskorn.kaml.Yaml
import com.pdaigle.potatobot.core.cli.CliBase
import com.pdaigle.potatobot.core.logging.Log
import sun.security.krb5.Config
import java.io.File

// This function needs to
// X Call something in ModuleHandler (or be passed a result from that being called elsewhere) to find all current modules
//      exists; can pass modules list from ConfigHandler as param
// X Generate a config file based on ConfigData
// X Populate that config file with ConfigData.Module objects with information from the ModuleHandler
class ConfigGenerate(configPath: String, modulesMap: List<ConfigData.Module>, logObj: Log): CliBase
{
    private val localPath = configPath
    private val defaultPaths: List<ConfigData.Path> = listOf(
        ConfigData.Path("config", "config.yaml"),
        ConfigData.Path("modules", "modules"),
        ConfigData.Path("token", "token.yaml"))
    private val localModules = modulesMap
    override val promptResponse = "File config.yaml is not present in PotatoBot's install directory. Would you like to auto-generate one? (Y/N): "
    override val logger = logObj
    private var localToken = ""

    override suspend fun execute(cmd: String, logger: Log)
    {
        when {
            cmd.toLowerCase().startsWith("y") -> {
                val output = "Generating new config.yaml file"
                val fileName = "config.yaml"
                // TODO: Create default path/structure; for now the file is generating in the same top-level directory as the jar
                //val filePath = localPath

                logger.logGeneric("", "INFO", output)
                val file = File(fileName)
                var moduleList = mutableListOf<ConfigData.Module>()
                var pathsList = mutableListOf<ConfigData.Path>()
                // iterate through modules in passed module list, add as ConfigData.Module objects to moduleList
                for (it in localModules)
                {
                    // should not include required modules as they are not configurable
                    println("Adding module ${it.name}")
                    // build ConfigData.Module object for current iterator
                    val dataModule = ConfigData.Module(
                        it.name,
                        it.isLoaded
                    )

                    moduleList.add(dataModule)
                }
                for (it in defaultPaths)
                {
                    println("Adding path variable for ${it.name}")
                    // build ConfigData.Path object for current iterator
                    val dataPath = ConfigData.Path(
                        it.name,
                        it.path
                    )

                    pathsList.add(dataPath)
                }

                logger.printCli("Would you like to enter your unique token now? (Y/N): ", false)
                val input = readLine()
                if (!input.isNullOrEmpty() && input.toLowerCase().startsWith("y"))
                {
                    logger.printCli("Enter your unique token: ", false)
                    val tokenInputLine = readLine()
                    println(tokenInputLine)
                    if (!tokenInputLine.isNullOrEmpty() && tokenInputLine.length > 58)
                        localToken = tokenInputLine
                    else
                        logger.logGeneric("ERROR", "ERROR", "Invalid token length. Please add your token manually to one of the following: " +
                                "config.yaml, token.yaml, or token.txt")
                }

                // write output list as string to file
                var outputConfig = ConfigData()
                outputConfig.modules = moduleList
                outputConfig.paths = pathsList
                if (!localToken.isNullOrEmpty() && localToken.length > 58)
                    outputConfig.token = localToken
                else
                    outputConfig.token = "badtoken"
                val result = Yaml.default.encodeToString(ConfigData.serializer(), outputConfig)
                file.writeText(result)

                println("Generated new config file with: ")
                println(result)

                logger.logCli(cmd, output)
            }
            cmd.toLowerCase().startsWith("n") -> {
                val output = "Please consult the documentation and create a config.yaml file in the same directory as this file."
                logger.logGeneric("", "INFO", output)
                logger.logCli(cmd, output)
            }
            else -> logger.logGeneric("ERROR", "ERROR", "Invalid option")
        }
    }
}