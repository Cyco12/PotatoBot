package com.pdaigle.potatobot.core.cli

import com.pdaigle.potatobot.core.logging.Log


// Contains basis for command line interfacing.
// Should include the following
// * Main function for cli input functionality
// * Response function
// * Default y/n overloadable functions
// Initial error checking can be limited, as this functionality will not necessarily be end-user facing, but accessed by hosts
//      for bot instances. Some degree of correct usage/knowledge will be assumed for the first version.
interface CliBase
{
    val promptResponse: String
    val logger: Log

    // Standardized input scanning/response for every command. Automatically calls execute() where the command should do all its logic
    suspend fun main()
    {
        // call execute here
        // dependencies/variables should be top level to the interface, not local to this function
        // TODO: Add checking for different types of CLI input; ie / commands for running Discord-facing commands
        //  from the terminal optionally vs just normal I/O
        if (promptResponse.isNotEmpty())
            prompt(promptResponse)
        else if (promptResponse.isEmpty())
        {
            val terminalInput = readLine()
            execute(terminalInput!!, logger)
        }
    }

    suspend fun prompt(promptParam: String)
    {
        logger.printCli(promptParam, false)
        val terminalInput = readLine()
        execute(terminalInput!!, logger)
    }

    // Commands should do anything other than text response here
    // this function should be overwritten by each command
    suspend fun execute(cmd: String, logger: Log)
    {
        // do overloadable stuff
        var output = "test test"

        // log stuff here
        logger.logCli(cmd, output)
    }
}