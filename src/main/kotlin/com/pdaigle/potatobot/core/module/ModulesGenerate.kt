package com.pdaigle.potatobot.core.module

import com.pdaigle.potatobot.core.cli.CliBase
import com.pdaigle.potatobot.core.logging.Log


// This function needs to
// X Call something in ModuleHandler (or be passed a result from that being called elsewhere) to find all current modules
//      exists; can pass modules list from ConfigHandler as param
// X Generate a config file based on ConfigData
// X Populate that config file with ConfigData.Module objects with information from the ModuleHandler
class ModulesGenerate(modulePath: String, logObj: Log): CliBase
{
    private val localPath = modulePath
    override val promptResponse = "The modules folder is not present in PotatoBot's install directory. Would you like to auto-generate one? (Y/N): "
    override val logger = logObj

    /*override suspend fun execute(cmd: String, logger: Log)
    {
        var prompt = println()
        if (cmd.toLowerCase().startsWith("y"))
        {
            var output = "Generating new config.yaml file"
            val fileName = "config.yaml"
            // TODO: Create default path/structure; for now the file is generating in the same top-level directory as the jar
            //val filePath = localPath

            println(output)
            var file = File(fileName)
            var moduleList = mutableListOf<ConfigData.Module>()
            // iterate through modules in passed module list, add as ConfigData.Module objects to moduleList
            for (it in localModules)
            {
                println("[debug]checking module ${it.name}")
                // should not include required modules as they are not configurable
                println("Adding module ${it.name}")
                // build ConfigData.Module object for current iterator
                val dataModule = ConfigData.Module(
                    it.name,
                    it.isLoaded
                )

                moduleList.add(dataModule)
            }
            // write output list as string to file
            var outputConfig = ConfigData()
            outputConfig.modules = moduleList
            val result = Yaml.default.encodeToString(ConfigData.serializer(), outputConfig)
            file.writeText(result)

            println("Generated new config file with: ")
            println(result)

            logger.logCli(cmd, output)
        }
        else if (cmd.toLowerCase().startsWith("n"))
        {
            var output = "Please consult the documentation and create a config.yaml file in the same directory as this file."
            println(output)
            logger.logCli(cmd, output)
        }
    }*/
}