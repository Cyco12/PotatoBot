package com.pdaigle.potatobot.core.module

import kotlinx.serialization.Serializable

@Serializable
data class ModuleData(
    var name: String,
    var isLoaded: Boolean,
    var isRequired: Boolean,
    var packageLocation: String,
    var commands: Array<String>
    )