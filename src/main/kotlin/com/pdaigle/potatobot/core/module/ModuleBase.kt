package com.pdaigle.potatobot.core.module

import com.pdaigle.potatobot.core.helper.HelpBase
import com.pdaigle.potatobot.core.logging.Log
import com.pdaigle.potatobot.core.command.CommandBase
import com.pdaigle.potatobot.core.ClientData
import com.pdaigle.potatobot.core.command.CommandData

import com.charleskorn.kaml.Yaml
import java.io.File

import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths


// Contains basis for modules. 
// * Includes parameter to be passed for ClientBase, 
// * Main function for command functionality
// * Response function
interface ModuleBase
{
    val yamlPath: String
    var modData: ModuleData
    var clientInfo: ClientData
    var helperObject: HelpBase
    var loggerObject: Log

    var commands: List<CommandBase>

    // Overrideable main() function. This is where calling commands will occur on a per module basis
    suspend fun main()
    {
        // log loaded commands and module information
        initializeCommands()
    }

    fun loadHelper()
    {
        helperObject.main()

        // locally cache command data objects
        val commandObjCache = HashMap<String, CommandData>()
        for (it in commands)
            commandObjCache[it.commandData.commandName.toLowerCase()] = it.commandData

        // add permissions to helper map from each command
        for (it in modData.commands)
            helperObject.permissions[it.toLowerCase()] = commandObjCache[it.toLowerCase()]!!.requiredPermissions + commandObjCache[it.toLowerCase()]!!.defaultPermissions
    }

    suspend fun initializeCommands()
    {
        for(item in commands)
            item.main()
    }

    fun getCommand(cmdName: String): CommandBase?
    {
        for(item in commands)
            if (item.commandData.commandName == cmdName.toLowerCase()) return item

        loggerObject.logGeneric("ERROR", "ERROR", "Command not found: $cmdName")
        return null
    }

    // Parses module data from yaml file using a String path
    fun parseModData(yamlPath: String): ModuleData // Using passed string to get File object for module object
    {
        // Getting module data YAML file as string
        val encoding = Charset.defaultCharset()
        val encoded = Files.readAllBytes(Paths.get(yamlPath))
        val fileAsString = String(encoded, encoding).trimIndent()

        //Serialize ModuleData object from yaml string and return it
        return Yaml.default.decodeFromString(ModuleData.serializer(), fileAsString)
    }

    // Parses module data from yaml file using a java.io.File
    fun parseModData(yamlFile: File): ModuleData // Using passed string to get File object for module object
    {
        // Getting module data YAML file as string
        val encoding = Charset.defaultCharset()
        val encoded = yamlFile.readBytes()
        val fileAsString = String(encoded, encoding).trimIndent()

        //Serialize ModuleData object from yaml string and return it
        return Yaml.default.decodeFromString(ModuleData.serializer(), fileAsString)
    }

    fun printModData()
    {
        println(modData.name)
        println(modData.isLoaded)
        println(modData.packageLocation)
    }

    fun printModData(dataParam: ModuleData)
    {
        println(dataParam.name)
        println(dataParam.isLoaded)
        println(dataParam.packageLocation)
    }
}