package com.pdaigle.potatobot.core.module

import com.pdaigle.potatobot.core.ClientData
import com.pdaigle.potatobot.core.config.ConfigHandler
import com.pdaigle.potatobot.core.logging.*
import com.pdaigle.potatobot.core.command.CommandBase


// Handles loading modules for the main bot file. Should do the following
// X Access config file (json or similar) to decide which modules to load
// X Run all instances of modules from main
// X ModuleHandler.main() being called from main bot file should load/run all necessary modules
// - Bonus: Download modules directly from Github instead of requiring all modules be installed locally?
class ModuleHandler(clientDataParam: ClientData)
{
    //var configName: String = configNameParam + ".json" // appends file extension to filename 
    private var localLogger: Log = Log()
    private var localClientData = clientDataParam
    private var config: ConfigHandler = ConfigHandler(localLogger, clientDataParam.paths)

    suspend fun loadModules()
    {
        loadRequired()
        config.main(localClientData) // make sure the config's map of modules is loaded
        val keysList = ArrayList(config.modules.keys)
        // loads any modules in config that have isLoaded set to true
        for (item in keysList)
        {
            val currentObj = config.modules.getValue(item.toString())
            if (currentObj.modData.isLoaded) currentObj.main()

            localLogger.logModule(currentObj.modData)
        }
    }

    private suspend fun loadRequired()
    {
        config.main(localClientData) // make sure the config's map of modules is loaded
        val keysList = ArrayList(config.required.keys)
        // loads any modules in config that have isLoaded set to true
        for (item in keysList)
        {
            val currentObj = config.required.getValue(item.toString())
            if (currentObj.modData.isLoaded) currentObj.main()

            localLogger.logReqModule(currentObj.modData)
        }
    }

    fun checkLoadedKeys(): MutableList<String>
    {
        config.main(localClientData) // make sure the config's map of modules is loaded
        val keysList = ArrayList(config.modules.keys)
        val loadedModules: MutableList<String> = ArrayList()
        // loads any modules in config that have isLoaded set to true
        for (item in keysList)
        {
            val currentObj = config.modules.getValue(item.toString())
            if (currentObj.modData.isLoaded) loadedModules.add(item)
        }

        return loadedModules
    }

    fun checkLoaded(): MutableList<ModuleBase>
    {
        config.main(localClientData) // make sure the config's map of modules is loaded
        val modsList = ArrayList(config.modules.values)
        // val reqModsList = ArrayList(config.required.values)
        val loadedModules: MutableList<ModuleBase> = ArrayList()
        // loads any modules in config that have isLoaded set to true
        for (item in modsList)
            if (item.modData.isLoaded) loadedModules.add(item)

        // add required modules
        checkRequired(loadedModules)

        return loadedModules
    }

    fun getCmd(cmdName: String): CommandBase?
    {
        val loadedModules = checkLoaded()

        for (module in loadedModules)
        {
            val localCmd = module.getCommand(cmdName)
            if(localCmd != null) return localCmd
        }

        return null
    }

    fun checkRequired(): Boolean
    {
        config.main(localClientData) // make sure the config's map of modules is loaded
        val reqList = ArrayList(config.required.values)
        // loads any modules in config that have isLoaded set to true
        for (item in reqList)
            if (!item.modData.isLoaded) 
                return false

        return true
    }
    private fun checkRequired(list: MutableList<ModuleBase>): MutableList<ModuleBase>
    {
        config.main(localClientData) // make sure the config's map of modules is loaded
        val reqList = ArrayList(config.required.values)
        // loads any modules in config that have isLoaded set to true
        for (item in reqList)
            if (item.modData.isLoaded) list.add(item)

        return list
    }
}