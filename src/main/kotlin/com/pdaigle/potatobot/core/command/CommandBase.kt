package com.pdaigle.potatobot.core.command

import java.time.Instant

import dev.kord.core.*
import dev.kord.core.event.message.*
import dev.kord.core.entity.*

import com.pdaigle.potatobot.core.ClientData


// Contains basis for commands. 
// * Includes parameter to be passed for prefix, 
// * Main function for command functionality
// * Response function
// * Command name/catchphrase
interface CommandBase
{
    val clientData: ClientData
    var commandData: CommandData

    enum class Response(val responseText: String)
    {
        Invalid_Permissions("Sorry, you lack the permissions required for this command."),
        Invalid_Parameters("That doesn't look quite right. Maybe try !help to figure out what arguments are accepted with this command."),
        Command_Mismatch("I don't recognize that command. Try !help to see a list of available commands and how to use them.")
    }

    // Standardized input scanning/response for every command. Automatically calls execute() where the command should do all its logic
    suspend fun main()
    {
        clientData.client.on<MessageCreateEvent> 
        {
            val msg = message.content.toLowerCase()

            if (msg.contains(clientData.prefix) && msg.contains(commandData.commandName))
            {
                val validationObject = CommandValidation(commandData, clientData, message)
                val argHandler = ArgsHandler(commandData, clientData, message)
                val localChecks = validationObject.getChecksList()

                if (!validationObject.isBot(localChecks))
                {
                    val eventTimestamp: Instant = Instant.now()

                    if (commandData.requiresPerms)
                    {
                        val permissionChecker = PermissionChecking(commandData)
                        permissionChecker.setReqPermissions(commandData.defaultPermissions)
                        val hasPerms = permissionChecker.hasReqPermissions(message)

                        if (hasPerms)
                        {
                            val localStates = validationObject.getStatesList(localChecks)
                            val cmdHasArgs = validationObject.hasArgsUsingStates(localStates)

                            if (cmdHasArgs)
                            {
                                if (localStates.contains(CommandValidation.CmdState.Can_Run))
                                {
                                    val arguments = argHandler.parseArgs()
                                    if (cmdHasArgs)
                                        execute(message, eventTimestamp, arguments)
                                }
                            }
                            else
                                execute(message, eventTimestamp)

                            if (localStates.contains(CommandValidation.CmdState.Has_Errored))
                            {
                                invalidCommand(message, validationObject.getErrorState())
                                clientData.logger.logGeneric("ERROR", "ERROR", validationObject.getErrorState())
                            }

                        }
                        if (!hasPerms)
                            invalidCommand(message, Response.Invalid_Permissions.responseText)
                    }
                    else
                    {
                        val localStates = validationObject.getStatesList(localChecks)
                        val cmdHasArgs = validationObject.hasArgsUsingStates(localStates)

                        if (cmdHasArgs)
                        {
                            if (localStates.contains(CommandValidation.CmdState.Can_Run))
                            {
                                val arguments = argHandler.parseArgs()
                                execute(message, eventTimestamp, arguments)
                            }
                        }
                        if (!cmdHasArgs)
                        {
                            if (localStates.contains(CommandValidation.CmdState.Can_Run))
                                execute(message, eventTimestamp)
                        }
                        
                        if (localStates.contains(CommandValidation.CmdState.Has_Errored))
                            invalidCommand(message, validationObject.getErrorState())
                    }
                }
            }
        }
    }

    // Commands should do anything other than text response here
    suspend fun execute(msgParam: Message, timestampParam: Instant)
    {
        // this function should be overwritten by each command (without args)
        msgParam.channel.createMessage(commandData.response) 

        // log stuff here
        clientData.logger.logCmd(commandData.commandName, msgParam)
    }

    suspend fun execute(msgParam: Message, timestampParam: Instant, msgArgs: String)
    {
        // this function should be overwritten by each command (with args)
        msgParam.channel.createMessage(commandData.response) 

        // log stuff here
        // should add an overload to this log method for logging with args
        clientData.logger.logCmd(commandData.commandName, msgParam, msgArgs)
    }

    suspend fun execute(msgParam: Message, timestampParam: Instant, msgArgs: ArgsData)
    {
        // this function should be overwritten by each command (with args from new args data class)
        msgParam.channel.createMessage(commandData.response) 

        // log stuff here
        // should add an overload to this log method for logging with args from new args data class
        clientData.logger.logCmd(commandData.commandName, msgParam, msgArgs)
    }

    //"Are you sure you typed that command correctly? Try !help if you're lost.")
    suspend fun invalidCommand(msgParam: Message, responseParam: String = Response.Command_Mismatch.responseText) 
    {
        // optionally override per command to include more command specific responses
        msgParam.channel.createMessage(responseParam)
    }

    // TODO: Correct null assertion for all commands that utilize this (and user objects in general)
    suspend fun getName(userObj: User?): String
    {
        if (userObj == null) { clientData.logger.logGeneric("ERROR", "ERROR", "No userObj param passed, or passed is null.") }

        when (userObj != null) {

            commandData.useMention -> return userObj!!.mention
            else                   -> return userObj!!.username
        }
    }
}