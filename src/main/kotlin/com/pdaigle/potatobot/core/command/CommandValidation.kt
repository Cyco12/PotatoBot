package com.pdaigle.potatobot.core.command

import com.pdaigle.potatobot.core.ClientData

import dev.kord.core.entity.Message


// Contains validation for commands. 
// * Has overwrite-able objects for CommandData and Message type objects
// * Enum for all valid cmdStates
// * Constructor/Collection for containing multiple cmdStates
// * Getter/Setter to return states without public access?
class CommandValidation(cmdDataParam: CommandData, clientDataParam: ClientData, msgParam: Message)
{
    val commandData: CommandData = cmdDataParam
    val clientData: ClientData = clientDataParam
    private val messageObject: Message = msgParam
    private val msgContent = messageObject.content.toLowerCase()
    private var errorState: ErrorState = ErrorState.Clean

    enum class CmdChecks
    {
        Author_Is_Bot,
        Msg_Exact_Match,
        Msg_Fuzzy_Match,
        Msg_Requires_Args,
        Msg_Accepts_Args,
        Msg_Length_Too_Short,
        Msg_Length_Too_Long
    }

    enum class ErrorState(val text: String)
    {
        Clean(""),
        Generic_Error("Error: Error unknown (this shouldn't happen)"),
        Missing_Args("Error: Missing Arguments"),
        Invalid_Command("Error: Invalid Command"),
        Is_Bot("Error: User is bot")
    }

    enum class CmdState(val error: ErrorState)
    {
        Can_Run(ErrorState.Clean),
        Has_Args(ErrorState.Clean),
        Has_Errored(ErrorState.Generic_Error),
        Invalid_State(ErrorState.Generic_Error)
    }

    private fun updateChecks(): MutableList<CmdChecks>
    {
        val checks: MutableList<CmdChecks> = mutableListOf()

        if (msgContent.contains(clientData.prefix))
        {
            val localCmd = clientData.prefix + commandData.commandName

            if (messageObject.author?.isBot == true) checks.add(CmdChecks.Author_Is_Bot)
            if (msgContent == localCmd) checks.add(CmdChecks.Msg_Exact_Match)
            if (!(msgContent == localCmd) && msgContent.contains(localCmd)) checks.add(CmdChecks.Msg_Fuzzy_Match)
            if (commandData.acceptsArgs) checks.add(CmdChecks.Msg_Accepts_Args)
            if (commandData.requiresArgs) checks.add(CmdChecks.Msg_Requires_Args)
            if (msgContent.length < localCmd.length && commandData.requiresArgs) checks.add(CmdChecks.Msg_Length_Too_Short)
            if (msgContent.length > localCmd.length && !commandData.requiresArgs) checks.add(CmdChecks.Msg_Length_Too_Long)
        }

        return checks
    }
    private fun updateStates(checks: MutableList<CmdChecks>): MutableList<CmdState>
    {
        val states: MutableList<CmdState> = mutableListOf()

        if (msgContent.contains(clientData.prefix))
        {       
            when
            {
                checks.contains(CmdChecks.Author_Is_Bot) -> {
                    states.add(CmdState.Has_Errored)
                    errorState = ErrorState.Is_Bot
                    return states 
                }
                checks.contains(CmdChecks.Msg_Exact_Match) -> {
                    if (checks.contains(CmdChecks.Msg_Requires_Args))
                    {
                        states.add(CmdState.Has_Errored)
                        errorState = ErrorState.Missing_Args
                        return states
                    }
                    else
                    {
                        states.add(CmdState.Can_Run)
                        return states
                    }
                }
                checks.contains(CmdChecks.Msg_Fuzzy_Match) -> {
                    if (checks.contains(CmdChecks.Msg_Accepts_Args) && !checks.contains(CmdChecks.Msg_Length_Too_Short))
                    {
                        states.add(CmdState.Can_Run)
                        states.add(CmdState.Has_Args)
                        return states
                    }
                    if (checks.contains(CmdChecks.Msg_Requires_Args) && checks.contains(CmdChecks.Msg_Length_Too_Short))
                    {
                        states.add(CmdState.Has_Errored)
                        errorState = ErrorState.Missing_Args
                        return states
                    }
                }
                /*else -> {
                    if (!states.contains(CmdState.Can_Run))
                    {
                        states.add(CmdState.Has_Errored)
                        errorState = ErrorState.Invalid_Command
                        println("no here")
                        return states
                    }
                }*/
            } 
        }
        return states
    }

    // slower, gets its own local list of CmdChecks by running updateChecks() again
    fun isBot(): Boolean
    {
        val localChecksList = getChecksList()
        if (localChecksList.contains(CmdChecks.Author_Is_Bot))
            return true
        return false
    }
    // faster, uses existing list of CmdChecks
    fun isBot(checksParam: MutableList<CmdChecks>): Boolean
    {
        val localChecksList = checksParam
        if (localChecksList.contains(CmdChecks.Author_Is_Bot))
            return true
        return false
    }

    // return error string based off error state
    fun getErrorState(): String { return errorState.text }

    fun getChecksList(): MutableList<CmdChecks>
    {
        val localList = updateChecks()
        return localList
    }
    fun getStatesList(checksParam: MutableList<CmdChecks>): MutableList<CmdState>
    { 
        val localList = updateStates(checksParam)
        return localList
    }
    
    // slower, gets its own local list of CmdChecks by running updateChecks() again
    fun getState(): CmdState
    {
        val localChecksList = updateChecks()
        val localStatesList = getStatesList(localChecksList)

        when
        {
            localStatesList.contains(CmdState.Has_Errored) -> {
                return CmdState.Has_Errored
            }
            localStatesList.contains(CmdState.Can_Run) -> {
                return CmdState.Can_Run
            }
            else -> {
                return CmdState.Invalid_State
            }
        } 
    }
    // faster, uses existing list of CmdChecks
    fun getState(checksParam: MutableList<CmdChecks>): CmdState
    {
        val localChecksList = checksParam
        val localStatesList = getStatesList(localChecksList)

        when
        {
            localStatesList.contains(CmdState.Has_Errored) -> {
                return CmdState.Has_Errored
            }
            localStatesList.contains(CmdState.Can_Run) -> {
                return CmdState.Can_Run
            }
            else -> {
                return CmdState.Invalid_State
            }
        } 
    }

    // slower, gets its own local list of CmdChecks by running updateChecks() again
    fun hasArgs(): Boolean 
    { 
        val localChecksList = updateChecks()
        val localStatesList = getStatesList(localChecksList)

        if (localStatesList.contains(CmdState.Has_Args)) 
            return true
        return false
    }
    // faster, uses existing list of CmdChecks
    fun hasArgs(checksParam: MutableList<CmdChecks>): Boolean 
    { 
        val localChecksList = checksParam
        val localStatesList = getStatesList(localChecksList)

        if (localStatesList.contains(CmdState.Has_Args)) 
            return true
        return false
    }
    // fastest, uses existing list of CmdStates
    fun hasArgsUsingStates(statesParam: MutableList<CmdState>): Boolean 
    { 
        val localStatesList = statesParam

        if (localStatesList.contains(CmdState.Has_Args)) 
            return true
        return false
    }
}