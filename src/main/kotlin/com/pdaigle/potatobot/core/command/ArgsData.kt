package com.pdaigle.potatobot.core.command

import dev.kord.common.entity.Snowflake

data class ArgsData(val snowflakes: List<Snowflake>, val flags: List<String>) { var genericArgs: MutableList<String> = mutableListOf() }