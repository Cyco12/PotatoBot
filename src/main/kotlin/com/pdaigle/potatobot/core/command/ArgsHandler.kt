package com.pdaigle.potatobot.core.command

import com.pdaigle.potatobot.core.ClientData

import dev.kord.common.entity.Snowflake
import dev.kord.core.entity.Message


class ArgsHandler(cmdDataParam: CommandData, clientDataParam: ClientData, msgParam: Message)
{
    val commandData: CommandData = cmdDataParam
    val clientData: ClientData = clientDataParam
    val messageObject: Message = msgParam
    private val msgContent = messageObject.content.toLowerCase()

    fun parseArgs(): ArgsData
    {
        val cmdLen = commandData.commandName.length
        val localArgs = msgContent.substring(cmdLen+1)
        var argsList: MutableList<String> = mutableListOf()
        val flagsList: MutableList<String> = mutableListOf()
        val mentionsList: MutableList<String> = mutableListOf()
        val snowflakesList: MutableList<Snowflake> = mutableListOf()
        val genericArgsList: MutableList<String> = mutableListOf()

        if (msgContent.contains("@"))
        {
            val totalStr = localArgs.split("-")
            val mentionArgs = totalStr[0]
            val splitMentions = mentionArgs.split("<@")
            var mntInd = 0
            while (mntInd < splitMentions.size)
            {
                if (mntInd == 0)
                    mntInd++
                var replacedItem = splitMentions[mntInd].replace(" ", "")
                replacedItem = replacedItem.replace(">", "")
                replacedItem = replacedItem.replace("!", "")
                mentionsList.add(replacedItem)

                snowflakesList.add(Snowflake(replacedItem.toLong()))

                mntInd++
            }
        }
        if (msgContent.contains("-"))
        {
            val totalStr = localArgs.split("-")
            val totalMentionsLen = totalStr[0].length
            val flagsArgs = localArgs.substring(totalMentionsLen+1)
            val splitFlags = flagsArgs.split("-")
            var flgInd = 0
            while (flgInd < splitFlags.size)
            {
                val replacedItem = "-" + splitFlags[flgInd]
                for (item in mentionsList)
                {
                    if (splitFlags[flgInd].contains(item))
                        flgInd++
                }
                flagsList.add(replacedItem)
                flgInd++
            }
        }
        else
        {
            val localGenericArgs = localArgs.substring(1).split(" ")
            for (arg in localGenericArgs)
                genericArgsList.add(arg)
        }

        val argsData = ArgsData(snowflakesList, flagsList)
        argsData.genericArgs = genericArgsList
        return argsData
    }
}