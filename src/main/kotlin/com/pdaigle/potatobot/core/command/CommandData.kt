package com.pdaigle.potatobot.core.command

import dev.kord.common.entity.Permission

data class CommandData(var commandName: String,
                       var response: String,
                       var invalidResponse: String,
                       var acceptsArgs: Boolean,
                       var requiresArgs: Boolean,
                       var requiresPerms: Boolean,
                       var useMention: Boolean,
                       val defaultPermissions: MutableList<Permission> = mutableListOf())
{ var requiredPermissions: MutableList<Permission> = mutableListOf() }