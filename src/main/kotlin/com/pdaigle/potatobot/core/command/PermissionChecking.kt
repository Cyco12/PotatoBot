package com.pdaigle.potatobot.core.command

import dev.kord.core.entity.*
import dev.kord.common.entity.Permission


// Contains basis for commands. 
// * Includes parameter to be passed for prefix, 
// * Main function for command functionality
// * Response function
// * Command name/catchphrase
class PermissionChecking(commandDataParam: CommandData)
{
    var commandData: CommandData = commandDataParam

    fun getReqPermissions(): List<Permission> { return commandData.requiredPermissions }
    fun getReqPermissions(dataParam: CommandData): List<Permission> { return dataParam.requiredPermissions }

    suspend fun hasReqPermissions(msgParam: Message): Boolean
    {
        val usr = msgParam.getAuthorAsMember()
        val usrPermissions = usr!!.getPermissions()
        val reqPermissions = getReqPermissions()

        val localHasPerms = reqPermissions.all{ it in usrPermissions }
        
        if (localHasPerms) 
            return true

        return false
    }

    suspend fun hasReqPermissions(dataParam: CommandData, msgParam: Message): Boolean
    {
        val usr = msgParam.getAuthorAsMember()
        val usrPermissions = usr!!.getPermissions()
        val reqPermissions = getReqPermissions(dataParam)
        val localHasPerms = reqPermissions.all{ it in usrPermissions }
        
        if (localHasPerms) 
            return true

        return false
    }

    fun setReqPermissions(newPermissions: MutableList<Permission>): CommandData
    { 
        for (item in newPermissions)
        {
            if (!commandData.requiredPermissions.contains(item))
                commandData.requiredPermissions.add(item)
        }

        return commandData
    }

    fun setReqPermissions(dataParam: CommandData, newPermissions: MutableList<Permission>): CommandData
    { 
        val localData = dataParam
        for (item in newPermissions)
        {
            if (!localData.requiredPermissions.contains(item))
                localData.requiredPermissions.add(item)
        }

        return localData
    }
}