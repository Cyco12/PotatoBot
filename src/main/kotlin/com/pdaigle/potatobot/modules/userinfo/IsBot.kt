package com.pdaigle.potatobot.modules.userinfo

import com.pdaigle.potatobot.core.command.CommandBase
import com.pdaigle.potatobot.core.command.CommandData
import com.pdaigle.potatobot.core.command.ArgsData
import com.pdaigle.potatobot.core.ClientData

import dev.kord.core.entity.Message

import java.time.Instant


class IsBot(clientDataParam: ClientData): CommandBase
{
    override val clientData = clientDataParam
    override var commandData: CommandData = CommandData("isbot", // command name
        "", // default response text
        "Are you sure you typed that command correctly? (Invalid arguments. Try mentioning a user)", // invalidResponse text
        acceptsArgs = true,
        requiresArgs = true,
        requiresPerms = false,
        useMention = true // if the response should use mentions(true) or plain text names (false)
        // mutableListOf(Permission.Administrator) // override default permissions required by command if necessary
    )

    override suspend fun execute(msgParam: Message, timestampParam: Instant, msgArgs: ArgsData)
    {        
        if (msgArgs.snowflakes.isNotEmpty())
        {
            println(msgArgs.snowflakes.size)
            for (item in msgArgs.snowflakes)
            {
                val target = clientData.client.getUser(item)
                val evalBoolean = target?.isBot
                val responseName = getName(target)
                if (evalBoolean == null || evalBoolean == false)
                    msgParam.channel.createMessage("$responseName **is not** a bot! Snowflake ID: $item")
                if (evalBoolean == true) 
                    msgParam.channel.createMessage("$responseName **is** a bot! Snowflake ID: $item")
            }         
            clientData.logger.logCmd(commandData.commandName, msgParam, msgArgs)
        }
        else invalidCommand(msgParam, commandData.invalidResponse)
    }
}