package com.pdaigle.potatobot.modules.userinfo

import com.pdaigle.potatobot.core.command.CommandBase
import com.pdaigle.potatobot.core.command.CommandData
import com.pdaigle.potatobot.core.command.ArgsData
import com.pdaigle.potatobot.core.ClientData

import dev.kord.core.entity.Message

import java.time.Instant


class CreatedOn(clientDataParam: ClientData): CommandBase
{
    override val clientData = clientDataParam
    override var commandData: CommandData = CommandData("createdon", // command name
        "", // default response text
        "Are you sure you typed that command correctly? (Invalid arguments. Try mentioning a user)", // invalidResponse text
        acceptsArgs = true,
        requiresArgs = false,
        requiresPerms = false,
        useMention = true // if the response should use mentions(true) or plain text names (false)
        // mutableListOf(Permission.Administrator) // override default permissions required by command if necessary
    )

    override suspend fun execute(msgParam: Message, timestampParam: Instant)
    {
        // get authors User object from msgParam then print avatar param from user object variable
        val createdDate = msgParam.author?.id?.timeStamp
        val authorName = getName(msgParam.author)
        msgParam.channel.createMessage("$authorName's account was created on: $createdDate")

        clientData.logger.logCmd(commandData.commandName, msgParam)
    }

    override suspend fun execute(msgParam: Message, timestampParam: Instant, msgArgs: ArgsData)
    {        
        if (msgArgs.snowflakes.isNotEmpty())
        {
            for (snowflake in msgArgs.snowflakes)
            {
                val target = clientData.client.getUser(snowflake)
                val createdDate = snowflake.timeStamp
                val mentionedName = getName(target)
                msgParam.channel.createMessage("$mentionedName's account was created on: $createdDate")
            }         
            clientData.logger.logCmd(commandData.commandName, msgParam, msgArgs)
        }
        else invalidCommand(msgParam, commandData.invalidResponse)
    }
}