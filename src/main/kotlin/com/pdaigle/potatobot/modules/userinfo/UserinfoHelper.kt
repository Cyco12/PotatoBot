// The Helper class for the userinfo module
package com.pdaigle.potatobot.modules.userinfo

import com.pdaigle.potatobot.core.helper.HelpBase
import com.pdaigle.potatobot.core.ClientData

import dev.kord.common.entity.Permission


class UserinfoHelper(clientParam: ClientData): HelpBase
{
    override var clientInfo = clientParam
    override var responses = HashMap<String, String>()
    override var permissions = HashMap<String, List<Permission>>()

    override fun main()
    {
        responses["createdon"] = "The ```!createdOn``` command will simply look up when the desired user's account was created for Discord. \n If it is called with a mention, the bot will check for the creation date of the mentioned user. \n This command can support multiple mentions at a time, but response time may vary (and the number of mentions may be limited per server)."
        responses["getavatar"] = "The ```!getAvatar``` command allows a user to get the link for their discord avatar. When the command is called, the bot will return a link to their Discord avatar. \n This command can also be called with mentions to get avatars of other users. \n The command can support multiple mentions at a time, but response time may vary (and the number of mentions may be limited per server)."
        responses["isbot"]     = "The ```!isBot``` command allows a user to check if a mentioned user is a bot. When the command is called, the bot will return whether or not the mentioned user is a bot. \n This command cannot be called without mentioning another user."
        responses["userinfo"]  = "The ```userinfo``` module currently contains the following commands ```createdOn, getAvatar, and isBot``` For command specific help, type ```!help command``` For example, ```!help isBot```"
    }
}