package com.pdaigle.potatobot.modules.util

import com.pdaigle.potatobot.core.command.CommandBase
import com.pdaigle.potatobot.core.command.CommandData
import com.pdaigle.potatobot.core.ClientData

import dev.kord.core.entity.Message

import java.time.Instant


class Ping(clientDataParam: ClientData): CommandBase
{
    override val clientData = clientDataParam
    override var commandData: CommandData = CommandData("ping", // command name
        "Pong!", // default response text
        "Are you sure you typed that command correctly? (Try typing !ping without any arguments or additional characters)", // invalidResponse text
        acceptsArgs = false,
        requiresArgs = false,
        requiresPerms = false,
        useMention = true // if the response should use mentions(true) or plain text names (false)
        // mutableListOf(Permission.Administrator) // override default permissions required by command if necessary
    )

    override suspend fun execute(msgParam: Message, timestampParam: Instant)
    {
        // get difference between command message timestamp (something like msgParam.timestamp) and current time when bot logs this command
        val executeTimestamp = timestampParam.toEpochMilli()
        val msgTimestamp = msgParam.timestamp.toEpochMilli()
        val responseTime = executeTimestamp - msgTimestamp

        msgParam.channel.createMessage("Pong! Response took " + responseTime + "ms")


        // log stuff here
        clientData.logger.logCmd(commandData.commandName, msgParam)
    }
}