package com.pdaigle.potatobot.modules.util

import com.pdaigle.potatobot.core.command.*
import com.pdaigle.potatobot.core.module.*
import com.pdaigle.potatobot.core.ClientData

import dev.kord.core.entity.Message

import java.time.Instant
import java.util.*


// TODO:
//    - Definitely going to want to format responses with embeds at some point; this is going to be a really ugly message without them
//    - Add the ability to check loaded modules based off of server-side config (server specific configs need to be implemented first)
//        (this is partially implemented, just not on a server by server basis; will require refactoring of how configs are handled)
//    - Add the ability to configure some parts of the response per-server
/*
channel.createEmbed {
            title = "t"
            color = Color.green
            author {
                name = "das"
            }

            footer {
                text = "dsa"
                icon = "url"
            }
        } */
class Help(clientDataParam: ClientData): CommandBase
{
    data class QueryResponse(
        var help: String,
        var permission: String
    )

    override val clientData = clientDataParam
    override var commandData: CommandData = CommandData("help", // command name
        "", // default response text
        "Unrecognized command/module. (Invalid arguments. Try !help to see your options)", // invalidResponse text
        acceptsArgs = true,
        requiresArgs = false,
        requiresPerms = false,
        useMention = true, // if the response should use mentions(true) or plain text names (false)
        // mutableListOf(Permission.Administrator) // override default permissions required by command if necessary
    )
    private var localHandler: ModuleHandler = ModuleHandler(clientData)

    override suspend fun execute(msgParam: Message, timestampParam: Instant)
    {
        // Show default help response
        val helpResponse = "Here is a list of things you can do with the help command: " +
                "\n - Determine what modules (groups of commands) are currently loaded/enabled on this server" +
                "\n - Figure out how to use a specified command \n For more information on specific commands/modules, " +
                "type '!help <command>'. For example, ```!help ping``` or ```!help util```"

        /*Here is a list of things you _can't_ do with the help command:
        - Take over the world
        - Talk to yourself
        - Fix that one obscure "feature" that's really just a bug
        ";*/
        val loadedModulesArr = localHandler.checkLoadedKeys()
        var loadedModules = ""
        for (item in loadedModulesArr)
            loadedModules += "[**$item**], "

        val authorName = getName(msgParam.author!!)
        msgParam.channel.createMessage("$authorName, $helpResponse")
        msgParam.channel.createMessage("\n\n The currently loaded modules are: $loadedModules")

        clientData.logger.logCmd(commandData.commandName, msgParam)
    }

    override suspend fun execute(msgParam: Message, timestampParam: Instant, msgArgs: ArgsData)
    {        
        val loadedModules = localHandler.checkLoaded() // collect module response sets from loaded modules
        var localQueryResponse: MutableList<QueryResponse> = mutableListOf()
        val responseList: HashMap<String, String> = HashMap<String, String>()
        val checkedResponseList: MutableList<String> = mutableListOf()
        val authorName = getName(msgParam.author)
        var checkedKeys = 0
        var commandFound = false

        for (module in loadedModules)
        {
            // compiler bug go brrrrrr - https://youtrack.jetbrains.com/issue/KT-22191 -
                // https://discuss.kotlinlang.org/t/platform-declaration-clash/12613/4
            /*val localResponses = module.helperObject.getResponses()

            for (responsePair in localResponses)
                responseList[responsePair.key] = responsePair.value*/
            
            module.loadHelper()

            for (response in module.helperObject.responses)
                responseList[response.key] = response.value
        }

        // fuzzy check args to see if the command/module name is in a list of all response keys from loaded modules
        while (!commandFound && checkedKeys <= responseList.size)
        {
            for (key in responseList.keys)
            {
                println("checking $key")
                if (msgArgs.genericArgs.contains(key))
                {
                    val localQuery = query(key, loadedModules)
                    localQueryResponse.add(localQuery)
                    checkedResponseList.add(key)
                    commandFound = true
                    break
                }
                checkedKeys++
            }
        }

        if (responseList.size != checkedKeys && !commandFound)
            clientData.logger.logGeneric("ERROR", "ERROR", "Command not found - Not Valid Key")
        if (commandFound && localQueryResponse.size == 1)
        {
            msgParam.channel.createMessage("$authorName, ${localQueryResponse[0].help}")
            msgParam.channel.createMessage(localQueryResponse[0].permission)
        }
        if (commandFound && localQueryResponse.size > 1)
        {
            msgParam.channel.createMessage("$authorName, there are multiple results for your query: ${checkedResponseList}.")
            for (queryResponse in localQueryResponse)
            {
                msgParam.channel.createMessage(queryResponse.help)
                msgParam.channel.createMessage(queryResponse.permission)
            }
        }
        else if (!commandFound)
            msgParam.channel.createMessage("$authorName, " +
                    "I can't seem to find any information about that. Try typing ```!help``` " +
                    "without any arguments to see which modules are currently loaded")

        clientData.logger.logCmd(commandData.commandName, msgParam, msgArgs)
    }

    private fun query(responseKey: String, modules: MutableList<ModuleBase>): QueryResponse
    {
        val localResponse = QueryResponse("", "")
        for (module in modules) 
        {
            module.loadHelper()
            if(module.helperObject.responses.contains(responseKey)) 
            {
                localResponse.help = module.helperObject.helpQuery(responseKey)
                if (!module.helperObject.permissions.isNullOrEmpty())
                {
                    println(module.helperObject.permissions.isNullOrEmpty())
                    println(module.helperObject.permissions)
                    localResponse.permission = module.helperObject.permissionQuery(responseKey)
                }

                return localResponse
            }
        }
        return localResponse
    }
}