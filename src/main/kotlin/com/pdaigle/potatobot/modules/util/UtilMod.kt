// The Module class for the util module
package com.pdaigle.potatobot.modules.util

import com.pdaigle.potatobot.core.module.*
import com.pdaigle.potatobot.core.helper.HelpBase
import com.pdaigle.potatobot.core.command.CommandBase
import com.pdaigle.potatobot.core.logging.Log
import com.pdaigle.potatobot.core.ClientData

import java.io.File


class UtilMod(clientParam: ClientData): ModuleBase
{
    override var clientInfo = clientParam
    override val yamlPath: String = listOf<String>(File("").absolutePath,
        clientInfo.paths.modules,
        "util",
        "moduledata.yaml").joinToString(File.separator)
    override var modData: ModuleData = parseModData(yamlPath)
    override var helperObject: HelpBase = UtilHelper(clientInfo)
    override var loggerObject: Log = clientInfo.logger
    override var commands: List<CommandBase> = listOf(Ping(clientInfo), Help(clientInfo))
}