// The Helper class for the util module
package com.pdaigle.potatobot.modules.util

import com.pdaigle.potatobot.core.helper.HelpBase
import com.pdaigle.potatobot.core.ClientData

import dev.kord.common.entity.Permission


class UtilHelper(clientParam: ClientData): HelpBase
{
    override var clientInfo = clientParam
    override var responses = HashMap<String, String>()
    override var permissions = HashMap<String, List<Permission>>()

    override fun main()
    {
        responses["ping"] = "The ```!ping``` command allows a user to test the responsiveness of PotatoBot. When the command is called, the bot will return how long it took to respond to the command in milliseconds. \n This number will include both network latency and the amount of time required for the bot to process the command."
        responses["util"] = "The ```util``` module currently contains the following commands ```ping, help``` For command specific help, type ```!help command``` For example, ```!help ping```"
    }
}