package com.pdaigle.potatobot.modules.admin

import com.pdaigle.potatobot.core.command.CommandBase
import com.pdaigle.potatobot.core.command.CommandData
import com.pdaigle.potatobot.core.command.ArgsData
import com.pdaigle.potatobot.core.ClientData

import dev.kord.core.entity.Message
import dev.kord.common.entity.Permission

import java.time.Instant


class Kick(clientDataParam: ClientData): CommandBase
{
    override val clientData = clientDataParam
    override var commandData: CommandData = CommandData("kick", // command name
        "", // default response text
        "Are you sure you typed that command correctly? (Invalid arguments. Try mentioning a user)", // invalidResponse text
        acceptsArgs = true,
        requiresArgs = true,
        requiresPerms = true,
        useMention = true, // if the response should use mentions(true) or plain text names (false)
        mutableListOf(Permission.KickMembers) // override default permissions required by command if necessary
    )

    override suspend fun execute(msgParam: Message, timestampParam: Instant, msgArgs: ArgsData)
    {        
        if (msgArgs.snowflakes.isNotEmpty())
        {
            for (item in msgArgs.snowflakes)
            {
                val localGuild = msgParam.getGuild()
                val target = clientData.client.getUser(item)
                val mentionedName = getName(target)

                localGuild.kick(item)
                msgParam.channel.createMessage("$mentionedName has been kicked.")
            }        
            clientData.logger.logCmd(commandData.commandName, msgParam, msgArgs)
        }
        else invalidCommand(msgParam, commandData.invalidResponse)
    }
}