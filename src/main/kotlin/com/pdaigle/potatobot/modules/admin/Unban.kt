package com.pdaigle.potatobot.modules.admin

import java.time.Instant

import dev.kord.core.entity.Message
import dev.kord.common.entity.Permission

import com.pdaigle.potatobot.core.command.*
import com.pdaigle.potatobot.core.ClientData
import dev.kord.core.entity.Invite
import dev.kord.core.entity.User
import dev.kord.core.entity.channel.GuildChannel
import kotlinx.coroutines.flow.first


class Unban(clientDataParam: ClientData): CommandBase
{
    override val clientData = clientDataParam
    override var commandData: CommandData = CommandData("unban", // command name
        "", // default response text
        "Are you sure you typed that command correctly? (Invalid arguments. Try mentioning a user)", // invalidResponse text
        acceptsArgs = true,
        requiresArgs = true,
        requiresPerms = true,
        useMention = true, // if the response should use mentions(true) or plain text names (false)
        mutableListOf(Permission.BanMembers) // override default permissions required by command if necessary
    )

    override suspend fun execute(msgParam: Message, timestampParam: Instant, msgArgs: ArgsData)
    {
        if (msgArgs.snowflakes.isNotEmpty())
        {
            /*var sendInvite = false
            var inviteFlagContent = ""

            for (flag in msgArgs.flags)
            {
                when
                {
                    flag.startsWith("-i") -> {
                        sendInvite = true
                        if (flag.length > 3)
                            inviteFlagContent = flag.substring(3)
                    }
                }
            }*/

            for (item in msgArgs.snowflakes)
            {
                val localGuild = msgParam.getGuild()
                val target = clientData.client.getUser(item)
                val responseName = getName(target)

                if (localGuild.getBanOrNull(item) != null)
                {
                    localGuild.unban(item)
                    msgParam.channel.createMessage("$responseName has been unbanned.")
                    /*println(sendInvite)
                    if (sendInvite)
                    {
                        var inviteCode = ""
                        if (inviteFlagContent.length > 3)
                        {
                            inviteUser(target, inviteFlagContent)
                            inviteCode = inviteFlagContent
                        }
                        else
                        {
                            val invite = getInvite(localGuild.getChannel(msgParam.channelId))
                            inviteUser(target, invite)
                            inviteCode = invite.code
                        }
                        msgParam.channel.createMessage("$responseName has been re-invited to the server using ```$inviteCode``` as the invite. (this doesn't work yet)")
                    }*/
                }
                else
                    msgParam.channel.createMessage("A ban does not exist for $responseName in this server. Make sure you are unbanning a valid user/ban.")
            }
            clientData.logger.logCmd(commandData.commandName, msgParam, msgArgs)
        }
        else invalidCommand(msgParam, commandData.invalidResponse)
    }

    private suspend fun inviteUser(targetUser: User?, targetInvite: Invite)
    {
        println("sending invite to dm as invite")
        if (targetUser != null) { targetUser.getDmChannel().createMessage("You have been unbanned from ${targetInvite.partialGuild!!.name}. Here is a new invite ${targetInvite.code}") }
        else if (targetUser == null) { clientData.logger.logGeneric("ERROR", "ERROR", "Error: targetUser param is null") }
    }
    private suspend fun inviteUser(targetUser: User?, targetInvite: String)
    {
        println("sending invite to dm as string")
        if (targetUser != null) { targetUser.getDmChannel().createMessage(targetInvite) }
        else if (targetUser == null) { clientData.logger.logGeneric("ERROR", "ERROR", "Error: targetUser param is null") }
    }

    private suspend fun getInvite(targetChannel: GuildChannel): Invite { return targetChannel.invites.first() }

    // TODO: add function to create invite using CategorizableChannel.createInvite when possible (#104)
}