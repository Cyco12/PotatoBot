package com.pdaigle.potatobot.modules.admin

import java.time.Instant

import dev.kord.core.entity.Message
import dev.kord.common.entity.Permission
import dev.kord.core.behavior.ban
import dev.kord.rest.builder.ban.BanCreateBuilder

import com.pdaigle.potatobot.core.command.*
import com.pdaigle.potatobot.core.ClientData
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract


class Ban(clientDataParam: ClientData): CommandBase
{
    override val clientData = clientDataParam
    override var commandData: CommandData = CommandData("ban", // command name
        "", // default response text
        "Are you sure you typed that command correctly? (Invalid arguments. Try mentioning a user)", // invalidResponse text
        acceptsArgs = true,
        requiresArgs = true,
        requiresPerms = true,
        useMention = true, // if the response should use mentions(true) or plain text names (false)
        mutableListOf(Permission.BanMembers) // override default permissions required by command if necessary
    )

    @ExperimentalContracts
    override suspend fun execute(msgParam: Message, timestampParam: Instant, msgArgs: ArgsData)
    {
        if (msgArgs.snowflakes.isNotEmpty())
        {
            for (snowflake in msgArgs.snowflakes)
            {
                val target = clientData.client.getUser(snowflake)
                val localGuild = msgParam.getGuild()
                val mentionedName = getName(target)
                var reason = ""
                var days = 0

                for (flag in msgArgs.flags)
                {
                    when
                    {
                        flag.startsWith("-r") -> {
                            val flagContent = flag.split(" ")
                            var reasonIndex = flagContent.indexOf("-r") + 1
                            while (reasonIndex < flagContent.size)
                            {
                                reason += " ${flagContent[reasonIndex]}"
                                reasonIndex++
                            }
                        }

                        flag.startsWith("-d") -> {
                            val splitMessage = msgParam.content.split(" ")
                            val deleteIndex = splitMessage.indexOf("-d") + 1
                            days = splitMessage[deleteIndex].toInt()
                        }
                    }
                }

                val localBanBuilder = BanCreateBuilder()
                if (reason.isNotEmpty()) localBanBuilder.reason = reason
                if (days > 0) localBanBuilder.deleteMessagesDays = days
                val builderContract = getBanBuilder { localBanBuilder }

                //localGuild.kick(target)
                localGuild.ban(snowflake, builderContract)
                msgParam.channel.createMessage("Poof! $mentionedName has been banned for the reason: '$reason'! Messages will be deleted for $days days.")
            }
            clientData.logger.logCmd(commandData.commandName, msgParam, msgArgs)
        }
        else invalidCommand(msgParam, commandData.invalidResponse)
    }

    @ExperimentalContracts
    fun getBanBuilder(builder: BanCreateBuilder.() -> Unit): BanCreateBuilder.() -> Unit {
        contract {
            callsInPlace(builder, InvocationKind.EXACTLY_ONCE)
        }
        return builder
    }
}