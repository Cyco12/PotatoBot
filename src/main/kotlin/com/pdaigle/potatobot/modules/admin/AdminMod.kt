// The Module class for the userinfo module
package com.pdaigle.potatobot.modules.admin

import com.pdaigle.potatobot.core.module.*
import com.pdaigle.potatobot.core.helper.HelpBase
import com.pdaigle.potatobot.core.command.CommandBase
import com.pdaigle.potatobot.core.logging.Log
import com.pdaigle.potatobot.core.ClientData

import java.io.File


class AdminMod(clientDataParam: ClientData): ModuleBase
{
    override var clientInfo = clientDataParam
    override val yamlPath: String = listOf<String>(File("").absolutePath,
        clientInfo.paths.modules,
        "admin",
        "moduledata.yaml").joinToString(File.separator)
    override var modData: ModuleData = parseModData(yamlPath)
    override var helperObject: HelpBase = AdminHelper(clientInfo)
    override var loggerObject: Log = clientInfo.logger
    override var commands: List<CommandBase> = listOf(Kick(clientInfo), Ban(clientInfo), Unban(clientInfo))
}