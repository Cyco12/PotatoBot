// The Helper class for the admin module
package com.pdaigle.potatobot.modules.admin

import com.pdaigle.potatobot.core.helper.HelpBase
import com.pdaigle.potatobot.core.ClientData

import dev.kord.common.entity.Permission


class AdminHelper(clientParam: ClientData): HelpBase
{
    override var clientInfo = clientParam
    override var responses = HashMap<String, String>()
    override var permissions = HashMap<String, List<Permission>>()

    override fun main()
    {
        responses["kick"] = "The ```!kick @user``` command will kick the mentioned user(s). This command cannot be called without a mention. " +
                "\n This command can support multiple mentions at a time, but response time may vary (and the number of mentions may be limited per server)."
        responses["ban"] = "The ```!ban @user``` command will ban the mentioned user(s). This command cannot be called without a mention. " +
                "\n The command can support multiple mentions at a time, but response time may vary (and the number of mentions may be limited per server)." +
                "\n This command can be used with the flags ```-r text explaining ban reason goes here``` for including a reason for the ban, and " +
                "```-d number``` for specifying the length of the ban in days."
        responses["unban"] = "The ```!unban @user``` command will unban the mentioned user(s). This command cannot be called without a mention. " +
                "\n The command can support multiple mentions at a time, but response time may vary (and the number of mentions may be limited per server)."
        responses["admin"]  = "The ```admin``` module currently contains the following commands ```kick, ban, and unban``` " +
                "\n For command specific help, type ```!help command``` For example, ```!help kick```"
    }
}